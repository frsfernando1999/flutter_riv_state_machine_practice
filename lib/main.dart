import 'package:flutter/material.dart';
import 'package:rive/rive.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Wolvie(),
    );
  }
}

class Wolvie extends StatefulWidget {
  const Wolvie({Key? key}) : super(key: key);

  @override
  State<Wolvie> createState() => _WolvieState();
}

class _WolvieState extends State<Wolvie> {
  SMIBool? _walk;
  SMIBool? _berserkerRage;
  SMIBool? _direction;

  void _onWolvieInit(Artboard artboard) {
    StateMachineController? controller =
        StateMachineController.fromArtboard(artboard, 'State Machine 1');
    artboard.addController(controller!);
    _walk = controller.findInput<bool>('walk') as SMIBool?;
    _berserkerRage = controller.findInput<bool>('berserkerRage') as SMIBool?;
    _direction =
        controller.findInput<bool>('direction(defaultRight)') as SMIBool?;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              height: 350,
              width: 350,
              child: RiveAnimation.asset(
                'assets/wolverine.riv',
                onInit: _onWolvieInit,
              ),
            ),
            ElevatedButton(onPressed: _hitWalk, child: const Text('Andar')),
            ElevatedButton(
                onPressed: _hitBerserk, child: const Text('Modo de batalha')),
            ElevatedButton(
                onPressed: _hitDirection, child: const Text('Mudar direção')),
          ],
        ),
      ),
    );
  }

  void _hitWalk() {
    bool? _walking = _walk?.value;
    if (_walking != null && _walking) {
      _walk?.value = false;
    } else {
      _walk?.value = true;
    }
  }

  void _hitBerserk() {
    bool? _enraged = _berserkerRage?.value;
    if (_enraged != null && _enraged) {
      _berserkerRage?.value = false;
    } else {
      _berserkerRage?.value = true;
    }
  }

  void _hitDirection() {
    bool? _walkDirection = _direction?.value;
    if (_walkDirection != null && _walkDirection) {
      _direction?.value = false;
    } else {
      _direction?.value = true;
    }
  }
}
