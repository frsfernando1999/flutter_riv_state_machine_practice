# Rive State Machine

This project was created to study the Rive State Machine integration on Flutter Projects.

To put it simply, you create any complex animation or animations on the Rive website, and, by using the Rive package, you can call methods that trigger the animations inside the application itself. 
